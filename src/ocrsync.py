#!/usr/bin/env python2
# -*- coding: utf-8 -*-

#  OCRSync - Keeps you updated with the latest OverClocked ReMixes
#
#  Version 2.0.4-beta
#
#  Copyleft 2012  - 2015  Felipe Peñailillo Castañeda (@breadmaker in identi.ca)
#                               <breadmaker@radiognu.org>
#                         Jorge Araya Navarro (@sweet in parlementum.net)
#                               <elcorreo@deshackra.com>
#                         Daniel Aguayo Catalán (@dantoac in identi.ca)
#                               <daniel@nim.io>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#  Complete info about licenses used and other legal stuff in COPYING file

import sys, platform, os, re
import ConfigParser, hashlib, HTMLParser, urllib
from random import choice

reload(sys)
sys.setdefaultencoding('utf-8')

# muestra un mensaje de excepcion personalizado, opcionalmente se puede indicar
# el nombre del modulo a instalar si este difiere del nombre con el que se importa
def showException(importName, *moduleName):
    print "Oops, %s is not installed." % importName
    print 'You can do this by running on a console "[sudo] pip install %s"' \
        % (moduleName or importName.lower())

try:
    import argparse
    global args
    argParser = argparse.ArgumentParser(description="Keeps you updated with"
        " the latest OverClocked ReMixes.", epilog="This program begins with the"
        " Qt GUI enabled by default.")
    argParser.add_argument("-t", "--text", action="store_true",
                                help="starts en text only mode")
    # notifications are *nix only for this release
    #argParser.add_argument("-n", "--notify", action="store_true",
    #                            help="notifies status on desktop (*nix only)")
    args = argParser.parse_args()
except ImportError, e:
    showException(str(e).rsplit(None, 1)[-1])
    exit(1)

# importando condicionalmente modulos dependiendo de los argumentos con los que
# se ha iniciado el programa
if not args.text:
    from locale import setlocale, LC_NUMERIC
    try:
        from PySide import __version__ as pyside_version
        from PySide.QtCore import __version__ as qtcore_version, Qt, SIGNAL, \
            SLOT, QCoreApplication, QRect, QSize, QMetaObject, QUrl
        from PySide.QtGui import QDesktopWidget, QMainWindow, QMessageBox, \
            QApplication, QFileDialog, QCursor, QWidget, QListWidgetItem, \
            QGridLayout, QLabel, QProgressBar, QSpacerItem, QSizePolicy
    except ImportError:
        showException("PySide", "pyside")
        exit(1)
    try:
        import pyside_excepthook
    except ImportError:
        print "You must have the file pyside_excepthook.py in the same folder" \
            " in order to start this application."
        exit(1)
    try:
        import ocrsync_ui
    except ImportError:
        print "You must have the file ocrsync_ui.py in the same folder in order" \
            " to start this application."
        exit(1)

# importando condicionalmente pynotify (no disponible en Win2)
# if platform.system() != "Windows":
#     try:
#         import pynotify
#     except ImportError, e:
#         showException(str(e).rsplit(None, 1)[-1])
#         exit(1)

# importando condicionalmente modulos que se deben instalar manualmente
try:
    from progressbar import *
except ImportError, e:
    showException(str(e).rsplit(None, 1)[-1])
    exit(1)

try:
    import BeautifulSoup
except ImportError, e:
    showException(str(e).rsplit(None, 1)[-1])
    exit(1)

try:
    import feedparser
except ImportError, e:
    showException(str(e).rsplit(None, 1)[-1])
    exit(1)

try:
    import requests
except ImportError, e:
    showException(str(e).rsplit(None, 1)[-1])
    exit(1)


# importacion fallida de ejemplo
# try:
#     import MyAsdf
# except ImportError, e:
#     showException(str(e).rsplit(None, 1)[-1], "relol")
#     exit(1)

__version__ = '2.0.4-beta'

if not args.text:
    sys.excepthook = pyside_excepthook.excepthook

config_folder = os.environ.get('XDG_CONFIG_HOME',
    os.path.join(os.path.expanduser('~'), ".config", "ocrsync"))
config = ConfigParser.ConfigParser()

def initConfig():
    if not os.path.exists(config_folder):
        os.makedirs(config_folder)
    config.read(os.path.join(config_folder, "config"))
    if not config.has_section("main"):
        config.add_section("main")
        config.set("main", "save_folder",
                os.environ.get('XDG_CONFIG_HOME', os.path.expanduser('~')))
        config.set("main", "last_ocremix", "3000")
        config.set("main", "own_folder", False)
        config.set("main", "own_folder_name", "OC ReMix - 3001 and beyond")
    else:
        if not config.has_option("main", "save_folder"):
            config.set("main", "save_folder",
                os.environ.get('XDG_CONFIG_HOME', os.path.expanduser('~')))
        if not config.has_option("main", "last_ocremix"):
            config.set("main", "last_ocremix", "3000")
        elif int(config.get("main", "last_ocremix")) < 3000:
            config.set("main", "last_ocremix", "3000")
        if not config.has_option("main", "own_folder"):
            config.set("main", "own_folder", False)
        if not config.has_option("main", "own_folder_name"):
            config.set("main", "own_folder_name", "OC ReMix - 3001 and beyond")
    saveConfig()

def saveConfig():
    with open(os.path.join(config_folder, "config"), "wb") as configfile:
        config.write(configfile)

def md5_for_file(f, file_md5, block_size=2**20):
    md5 = hashlib.md5()
    while True:
        data = f.read(block_size)
        if not data:
            break
        md5.update(data)
    return file_md5 == md5.hexdigest()

def size_for_file(file, size):
    statinfo = os.stat(file)
    return size == statinfo.st_size

def notifyMessage(title, message):
    if platform.system() != "Windows":
        n = pynotify.Notification(title, message, "res/icon.png")
        n.set_icon_from_pixbuf(ocrIcon)
        n.show()

def updateStatus(message):
    print message
    if not args.text:
        frame.setStatusBarMessage(message)
        QCoreApplication.instance().processEvents()

downloading_remix = False

def startSync():
    if not args.text:
        frame.toggleControls(False)
        frame.clearDetailsText()
        QApplication.setOverrideCursor(Qt.WaitCursor)
    updateStatus("Checking synchronization status")

    # Obteniendo el RSS
    fuente = feedparser.parse("http://ocremix.org/feeds/ten091/")
    if not args.text:
        QApplication.restoreOverrideCursor();
        QApplication.instance().processEvents()

    # Chequeando por errores de conexión de feedparser
    if fuente is None or not hasattr(fuente,"status"):
        #if args.notify:
        #    notifyMessage("OCRSync: Error", "Can't connect to the RSS source")
        if not args.text:
            QMessageBox.critical(frame, "Oops, we have an error!",
                "Can't connect to the RSS source. Check your connection and"
                " try again later.")
            updateStatus("Error: Can't connect to the RSS source")
            frame.toggleControls(True)
            return
        else:
            raise Warning("Can't connect to the RSS source. Check your "
                "connection and try again later.")
    if fuente.status != 200:
        #if args.notify:
        #    notifyMessage("OCRSync: Error",
        #        "HTTP error %d when trying to connect" % fuente.status)
        if not args.text:
            QMessageBox.critical(frame, "Oops, we have an error!",
                "HTTP error %d when trying to connect. Try again later." \
                % fuente.status)
            updateStatus("Error: HTTP error %d when trying to connect" \
                % fuente.status)
            frame.toggleControls(True)
            return
        else:
            raise Warning("HTTP error %d when trying to connect. Try again"
                " later."  % fuente.status)

    # Obtenemos el int desde el url de la última entrada del RSS
    lastOCR = int(fuente["entries"][0]["link"][32:-1])

    currentOCR = int(config.get("main", "last_ocremix"))

    newOCR = lastOCR - currentOCR

    if newOCR != 0:
        print "Unsynchronized: Last song saved is No. %d (That's %d new song%s)" \
            % (currentOCR, newOCR, "s" if newOCR != 1 else "")

    if currentOCR < lastOCR:
        if not args.text:
            frame.setStatusBarMessage("%d new song%s" % \
                (newOCR, "s" if newOCR != 1 else ""))
            QApplication.instance().processEvents()
        #if args.notify:
        #    notifyMessage("OCRSync: Starting sincronization",
        #        "%d new song%s." % (newOCR, "s" if newOCR != 1 else ""))
        global downloading_remix
        downloading_remix = True
        # Bajamos los remixes.
        # Si el remix no existe, se devuelve un html con código 404, de nombre
        # 404.html, que por supuesto no incluye los enlaces para descargar,
        # si no hay enlaces, no se descarga nada.
        # Chequear cómo se resolvió eso en http://pastebin.com/f1w3G6cr
        for remix in range(currentOCR + 1, lastOCR + 1):
            if not args.text:
                frame.setStatusBarMessage("Preparing download for song No. %d" \
                    % remix)
                QApplication.instance().processEvents()
            completed_percentage = float(remix-1) / float(lastOCR) * 100
            print "Preparing download for song No. %d... Sync status: " \
                "%.2f%%" % (remix, completed_percentage)
            # Pedimos la pagina web donde reside el remix
            remix_url = "http://ocremix.org/remix/OCR0%s/" % str(remix).zfill(4)
            if not args.text:
                frame.addRemixToDetails(remix_url.split("/")[-2], remix_url)
                frame.setCurrentProgressText("Preparing")
                frame.setProgress(completed_percentage)
                QApplication.instance().processEvents()
            ocremix_web = requests.get(remix_url,
                headers = {"User-Agent": "OCRSync %s python-requests/%s %s" \
                % (__version__, requests.__version__, platform.system())})
            # Procesamos las etiquetas HTML con BeautifulSoup
            # Hagamos la sopa!!
            ocremix_web_bs = BeautifulSoup.BeautifulSoup(ocremix_web.content)
            if not args.text:
                QApplication.instance().processEvents()
            enlaces = []
            remix_md5 = ""
            remix_size = 0
            # Buscando y guardando los enlaces
            for tag in ocremix_web_bs.findAll("a"):
                if not args.text:
                    QApplication.instance().processEvents()
                if ".mp3" in tag["href"]:
                    if not "iterations.org" in tag["href"]:
                        enlaces.append(tag["href"])
            # Buscando y guardando el MD5 y el tamaño
            for tag in ocremix_web_bs.findAll("li"):
                for content in tag.contents:
                    if "MD5" in str(content.string):
                        remix_md5 = (str(tag.contents[2]))
                    if "Size" in str(content.string):
                        remix_size = int(tag.contents[2].split(" ")[0].replace(",",""))
                if not args.text:
                    QApplication.instance().processEvents()
            if remix_md5 == "" or not re.findall(r"([a-fA-F\d]{32})", remix_md5):
                raise RuntimeError("Unable to get the MD5 of the file.")
            # Con choice() escogemos solo uno de los hostings para descargar el remix
            remix_in_dl = choice(enlaces)
            # Debemos asegurarnos de que el texto se muestre correctamente
            h = HTMLParser.HTMLParser()
            downloadRemix(h.unescape(ocremix_web_bs.head.title.string[:-20]), \
                remix_url, remix_in_dl, remix_md5, remix_size, remix)
        downloading_remix = False
        updateStatus("100% synchronized. Good work, Desmond.")
        if not args.text:
            frame.setProgress(100)
            frame.toggleControls(True)
        #if args.notify:
        #    notifyMessage("OCRSync: 100% synchronized",
        #        "Synchronization completed.")

        # Una vez terminada la descarga, guardamos el número del último remix
        config.set("main", "last_ocremix", str(lastOCR))
        saveConfig()
    else:
        updateStatus("100% synchronized. Good work, Desmond.")
        if not args.text:
            frame.setProgress(100)
            frame.toggleControls(True)
        #if args.notify:
        #    notifyMessage("OCRSync: 100% synchronized",
        #        "No new OverClocked ReMixes.")

def downloadRemix(remix_title, remix_url, remix_in_dl, remix_md5, remix_size, remix_number):
    # Chequeamos si el directorio donde se va a descargar existe para crearlo
    # si es necesario
    song_filename = os.path.join(unicode(config.get("main", "save_folder")),
            os.path.basename(urllib.unquote(remix_in_dl)))
    if not os.path.exists(os.path.abspath(os.path.join(song_filename, os.path.pardir))):
        os.makedirs(os.path.abspath(os.path.join(song_filename, os.path.pardir)))
    # Descargamos el remix en cuestión
    dl_complete = False
    while not dl_complete:
        # Verificamos si el archivo existe antes de crearlo
        if os.path.isfile(song_filename):
            # Si el archivo existe, es necesario verificar si se trata de
            # un archivo descargado a medias
            song = open(song_filename, "rb")
            # Es necesario verificar si no hay un error en el MD5 declarado
            # en el sitio, este es un workaround para la incidencia #14
            if md5_for_file(song, remix_md5) or size_for_file(song_filename, remix_size):
                # Como el md5 es correcto, es un archivo completo, por lo
                # tanto, continuamos con la siguiente cancion
                config.set("main", "last_ocremix", str(remix_number))
                saveConfig()
                print "The %s has already been downloaded, skipping..." \
                    % remix_title
                if not args.text:
                    frame.updateCurrentProgress(100)
                    frame.setCurrentProgressText("Exists")
                    frame.setCurrentLabelText("<a href='%s'>%s</a>" \
                        % (remix_url, remix_title[7:]))
                    frame.setStatusBarMessage("%s already downloaded, skipping..." \
                        % remix_title)
                    QApplication.instance().processEvents()
                return
            else:
                song.close()
                print "Fetching %s (%s)" % (remix_title, remix_url)
                if not args.text:
                    frame.setStatusBarMessage("Fetching %s" % remix_title)
                    frame.setCurrentProgressText("Fetching")
                    frame.setCurrentLabelText("<a href='%s'>%s</a>" \
                        % (remix_url, remix_title[7:]))
                    QApplication.instance().processEvents()
        else:
            print "Fetching %s (%s)" % (remix_title, remix_url)
            if not args.text:
                frame.setStatusBarMessage("Fetching %s" % remix_title)
                frame.setCurrentProgressText("Fetching")
                frame.setCurrentLabelText("<a href='%s'>%s</a>" \
                    % (remix_url, remix_title[7:]))
                QApplication.instance().processEvents()
        song = open(song_filename, "wb")
        # Hacemos get usando stream para obtener sólo la cabecera
        cancion_req = requests.get(remix_in_dl, stream = True,
            headers = {"User-Agent": "OCRSync %s python-requests/%s %s" \
            % (__version__, requests.__version__, platform.system())})
        # size almacena el tamaño a descargar
        # bytes_w almacena los bytes escritos
        size = int(cancion_req.headers['Content-Length'].strip())
        bytes_w = 0
        # Widgets para ProgressBar
        widgets = ["Downloading %.1fMB: " % float(float(size) / 1024 / 1024),
            Bar(marker=RotatingMarker(), left="[", right="]"),
            Percentage(), " ", FileTransferSpeed()]
        # Inicializando ProgressBar
        pbar = ProgressBar(widgets=widgets, maxval=size).start()
        if not args.text:
            frame.resetCurrentProgressTextFormat()
            frame.setStatusBarMessage("Downloading %s" % remix_title)
            QApplication.instance().processEvents()
        # Ahora si comenzamos la descarga, actualizamos por cada 1024 bytes
        for buf in cancion_req.iter_content(1024):
            if buf:
                song.write(buf)
                bytes_w += len(buf)
                pbar.update(bytes_w)
            if not args.text:
                frame.updateCurrentProgress(int(pbar.percentage()))
                QApplication.instance().processEvents()
        pbar.finish()
        song.close()
        song = open(song_filename, "rb")
        # Si el MD5 es correcto, se actualiza .lastocr
        # Esto sirve para cuando el usuario quiera interrumpir la descarga
        # para continuar más tarde
        if md5_for_file(song, remix_md5) or size_for_file(song_filename, remix_size):
            config.set("main", "last_ocremix", str(remix_number))
            saveConfig()
            dl_complete = True
            if not args.text:
                frame.setCurrentProgressText("Done")
                QApplication.instance().processEvents()
        else:
            print "Error en la suma de comprobación, el remix se " \
                "descargará nuevamente"
        song.close()

if not args.text:
    class CustomProgress(QWidget):
        def __init__(self, widgetText, parent=None):
            super(CustomProgress, self).__init__(parent)
            self.layoutWidget = QWidget()
            self.layoutWidget.setGeometry(QRect(0, 0, 611, 31))
            self.layoutWidget.setObjectName("layoutWidget")
            self.gridLayout = QGridLayout(self.layoutWidget)
            self.gridLayout.setContentsMargins(3, 3, 3, 3)
            self.gridLayout.setSpacing(3)
            self.gridLayout.setObjectName("gridLayout")
            self.label = QLabel(self.layoutWidget)
            self.label.setOpenExternalLinks(True)
            self.label.setObjectName("label")
            self.connect(self.label, SIGNAL('clicked()'), self.labelClicked)
            self.progressBar = QProgressBar(self.layoutWidget)
            self.progressBar.setProperty("value", 0)
            self.progressBar.setObjectName("progressBar")
            self.progressBar.setMinimumSize(QSize(75, 0))
            self.progressBar.setMaximumSize(QSize(75, 16777215))
            spacerItem = QSpacerItem(40, 20, QSizePolicy.Expanding,
                QSizePolicy.Minimum)
            self.gridLayout.addWidget(self.progressBar, 0, 0, 1, 1)
            self.gridLayout.addWidget(self.label, 0, 1, 1, 1)
            self.gridLayout.addItem(spacerItem, 0, 2, 1, 1)

            QMetaObject.connectSlotsByName(self)
            self.label.setText(widgetText)

            self.setLayout(self.gridLayout)

        def labelClicked(self):
            print "clicked!"
            QDesktopServices.openUrl(QUrl(self.label.text().split("'")[1],
                QUrl.TolerantMode))

    class MainWindow(QMainWindow, ocrsync_ui.Ui_MainWindow):
        def __init__(self, parent=None):
            super(MainWindow, self).__init__(parent)
            self.setupUi(self)
            self.center()
            self.applyConfig()
            self.setWindowTitle("OCRSync %s" % __version__)
            self.browseButton.clicked.connect(self.openFolderSelectDialog)
            self.ownFolderCheckBox.clicked.connect(self.toggleOwnFolder)
            self.detailsButton.clicked.connect(self.toggleDetails)
            self.aboutButton.clicked.connect(self.aboutBox)
            self.syncButton.clicked.connect(startSync)
            self.connect(self.ownFolderLineEdit, SIGNAL("textChanged(QString)"),
                self, SLOT("ownFolderTextChanged()"))

        def applyConfig(self):
            self.selectPathLineEdit.setText(unicode(config.get("main",
                "save_folder")))
            self.ownFolderCheckBox.setChecked(config.get("main", "own_folder") \
                == 'True')
            self.ownFolderLineEdit.setEnabled(self.ownFolderCheckBox.isChecked())
            self.ownFolderLineEdit.setText(unicode(config.get("main",
                "own_folder_name")))

        def ownFolderTextChanged(self):
            self.selectPathLineEdit.setText(os.path.abspath(os.path.join( \
                self.selectPathLineEdit.text(), os.pardir)))
            self.selectPathLineEdit.setText(os.path.join( \
                self.selectPathLineEdit.text(), self.ownFolderLineEdit.text()))
            config.set("main", "save_folder", self.selectPathLineEdit.text())
            config.set("main", "own_folder_name", self.ownFolderLineEdit.text())
            saveConfig()

        def closeEvent(self, event):
            global downloading_remix
            if downloading_remix:
                reply = QMessageBox.warning(self, "Heads up!",
                    "You are in the middle of a sync. If you close the program,"
                    " the song that is downloading right now will stay"
                    " incomplete. Are you sure you want to exit the program?",
                    QMessageBox.Yes, QMessageBox.No)
                if reply == QMessageBox.Yes:
                    event.accept()
                    sys.exit(0)
                else:
                    event.ignore()

        def updateCurrentProgress(self, percentage):
            item = self.detailsListWidget.item(self.detailsListWidget.count()-1)
            currentItem = self.detailsListWidget.itemWidget(item)
            currentItem.progressBar.setValue(percentage)
            QCoreApplication.instance().processEvents()

        def setCurrentProgressText(self, message):
            item = self.detailsListWidget.item(self.detailsListWidget.count()-1)
            currentItem = self.detailsListWidget.itemWidget(item)
            currentItem.progressBar.setFormat(message)
            QCoreApplication.instance().processEvents()

        def resetCurrentProgressTextFormat(self):
            item = self.detailsListWidget.item(self.detailsListWidget.count()-1)
            currentItem = self.detailsListWidget.itemWidget(item)
            currentItem.progressBar.setFormat("%p%")
            QCoreApplication.instance().processEvents()

        def setCurrentLabelText(self, message):
            item = self.detailsListWidget.item(self.detailsListWidget.count()-1)
            currentItem = self.detailsListWidget.itemWidget(item)
            currentItem.label.setText(message)
            currentItem.label.setToolTip(message.split(">")[1].split("<")[0])
            QCoreApplication.instance().processEvents()

        def center(self):
            qr = self.frameGeometry()
            cp = QDesktopWidget().availableGeometry().center()
            qr.moveCenter(cp)
            self.move(qr.topLeft())

        def setStatusBarMessage(self, message):
            self.statusbar.showMessage(message)

        def setProgress(self, number):
            self.progressBar.setValue(number)

        def toggleControls(self, isEnabled):
            self.selectPathLineEdit.setEnabled(isEnabled)
            self.browseButton.setEnabled(isEnabled)
            self.ownFolderCheckBox.setEnabled(isEnabled)
            self.ownFolderLineEdit.setEnabled(isEnabled)
            self.syncButton.setEnabled(isEnabled)

        def toggleDetails(self):
            if self.detailsListWidget.height() == 0:
                self.detailsButton.setText(u"\u25be &Details")
                self.setMinimumHeight(self.minimumHeight() + 100)
                self.setMaximumHeight(self.maximumHeight() + 100)
                self.detailsListWidget.setMinimumHeight(100)
                self.detailsListWidget.setMaximumHeight(100)
            else:
                self.detailsButton.setText(u"\u25b8 &Details")
                self.setMinimumHeight(self.minimumHeight() - 100)
                self.setMaximumHeight(self.maximumHeight() - 100)
                self.detailsListWidget.setMinimumHeight(0)
                self.detailsListWidget.setMaximumHeight(0)

        def clearDetailsText(self):
            self.detailsListWidget.clear()

        def addRemixToDetails(self, title, url):
            item = QListWidgetItem(self.detailsListWidget)
            item_widget = CustomProgress("<a href='%s'>%s</a>" \
                % (url, title))
            item.setSizeHint(item_widget.sizeHint())
            self.detailsListWidget.addItem(item)
            self.detailsListWidget.setItemWidget(item, item_widget)
            self.detailsListWidget.scrollToBottom()

        def openFolderSelectDialog(self):
            '''Opens the file explorer.'''
            result = QFileDialog.getExistingDirectory(self,
                "Select a target folder",
                os.environ.get('XDG_CONFIG_HOME', os.path.expanduser('~')))
            if result:
                if self.ownFolderCheckBox.isChecked():
                    self.selectPathLineEdit.setText(os.path.join(result,
                        self.ownFolderLineEdit.text()))
                else:
                    self.selectPathLineEdit.setText(result)
                config.set("main", "save_folder", self.selectPathLineEdit.text())
                saveConfig()

        def toggleOwnFolder(self):
            self.ownFolderLineEdit.setEnabled(self.ownFolderCheckBox.isChecked())
            if (self.selectPathLineEdit.text()):
                if self.ownFolderCheckBox.isChecked():
                    self.selectPathLineEdit.setText(os.path.join( \
                        self.selectPathLineEdit.text(),
                        self.ownFolderLineEdit.text()))
                else:
                    self.selectPathLineEdit.setText(os.path.abspath( \
                        os.path.join(self.selectPathLineEdit.text(),
                        os.pardir)))
                config.set("main", "save_folder", self.selectPathLineEdit.text())
            config.set("main", "own_folder", self.ownFolderCheckBox.isChecked())
            saveConfig()

        def aboutBox(self):
            '''Popup a box with about message.'''
            QMessageBox.about(self, "About OCRSync",
                """<p>OCRSync v%s - Keeps you updated with the latest OverClocked
                ReMixes</p>
                <p>Copyright &copy; 2012 - 2015 Felipe Pe&ntilde;ailillo, Jorge
                Araya, Daniel Aguayo</p>
                <p>This program is free software: you can redistribute it and/or
                modify it under the terms of the GNU General Public License as
                published by the Free Software Foundation, either version 3 of
                the License, or (at your option) any later version.</p>
                <p>This program is distributed in the hope that it will be
                useful, but WITHOUT ANY WARRANTY; without even the implied
                warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
                See the GNU General Public License for more details.</p>
                <p>You should have received a copy of the GNU General Public
                License along with this program.  If not, see
                <a href='http://www.gnu.org/licenses/gpl.html'>
                http://www.gnu.org/licenses/gpl.html</a>.</p>
                <p>OverClocked Remix and its logo are copyright
                <a href='http://ocremix.org/'>OverClocked ReMix</a>, LLC.<br/>
                Please read the Terms of Use in
                <a href='http://ocremix.org/info/Content_Policy'>
                http://ocremix.org/info/Content_Policy</a></p>
                <p>"Assassin's Creed" is a trademark of Ubisoft Entertainment in
                the U.S. and/or other countries. &copy; 2007</p>
                <p>The Legend of Zelda<sup>&reg;</sup> A Link To The
                Past<sup>&reg;</sup> are trademarks of Nintendo Company, Ltd.
                &copy; 1992.</p>
                <p>Earthbound<sup>&reg;</sup> is atrademark of Nintendo Company,
                Ltd. &copy; 1995.</p>
                <p>Final Fantasy IV<sup>&reg;</sup> is a trademark of Square
                Company, Ltd. &copy; 1994.</p>
                <p>Donkey Kong Country<sup>&reg;</sup> is a trademark of
                Nintendo of America, Inc. &copy; 1994.</p>
                <hr/>
                <p><i>Running in %s, using Python v%s, PySide v%s and Qt
                v%s</p></i>""" % (__version__, platform.system(),
                platform.python_version(), pyside_version, qtcore_version))

if __name__ == '__main__':
    initConfig()
    #if args.notify:
    #    if platform.system() == "Windows":
    #        print "Notifications are for the moment *nix only"
    #    else:
    #        # Inicializando el notificador
    #        pynotify.init("OCRSync")
    if args.text:
        startSync()
    else:
        global frame
        app = QApplication(sys.argv)
        setlocale(LC_NUMERIC, "C")
        app.setApplicationName("OCRSync")
        frame = MainWindow()
        frame.show()
        app.exec_()