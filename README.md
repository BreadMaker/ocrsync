# OCRSync #

---

## Standalone binaries available in the `dist` folder ##

### *Last stable release:* 2.0.4-beta ###

Simple and reliable tool written in [python](http://oython.org), made to easily
keep you in sync with the musical repository of [ocremix.org](http://ocremix.org).

**Now with graphical interface!***

![ocrsync.png](https://bitbucket.org/repo/574Xzd/images/2634364577-ocrsync.png)

**Results may vary, depending on the system in which it is run.*

## Building ##

In Debian-like distros, you need to execute the following command:

```
#!bash
$ sudo apt-get install cmake qt4-qmake python-dev libxml2-dev libxslt1-dev libqt4-gui libqt4-core libqt4-xml libqt4-dev
```

If you haven't, you must install `virtualenv` and `pip`. Next, install the
needed packages using `requirements.txt`:

```
#!bash
$ sudo apt-get install python-virtualenv python-pip
$ cd /path/to/ocrsync
$ virtualenv venv
$ source venv/bin/activate
(venv)$ pip install -r requirements.txt
```

### Installing pyinstaller ###

To install pyinstaller, you need v2.2+, for the meantime, you have to install
`develop` branch from github:

```
#!bash
(venv)$ pip install https://github.com/pyinstaller/pyinstaller/archive/develop.zip
```

And then make the standalone executable with:

```
#!bash
$ cd /path/to/ocrsync
$ source venv/bin/activate
(venv)$ venv/bin/pyinstaller -F src/ocrsync.py
```

More info in the [Wiki](https://bitbucket.org/BreadMaker/ocrsync/wiki)

## Changelog ##

* 2.0.4-beta
    * Workaround when the MD5 string doesn't match, #14.
* 2.0.3-beta
    * App layout rebuilt from scratch.
* 2.0.2-beta
    * Now syncs from song 3001.
    * Extended support for RSS errors.
    * Virtualenv improvements.
    * Pyinstaller is now fetched from dev.